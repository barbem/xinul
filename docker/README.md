# Docker PCSEA RISC-V

L'image Docker attachée au projet PCSEA RISC-V permet de profiter
rapidement d'un environnement de cross-compilation et de simulation fonctionnel pour
l’architecture RISC-V.

Avec cette image il est possible de :

* Cross-compiler le système d'exploitation, parties user et kernel;
* simuler le kernel avec Qemu;
* débuger le kernel avec GDB et Qemu;
* cross-compiler un programme RISC-V elf et l’exécuter sur une machine
host grâce à Spike et PK;
* lancer un bash pour utiliser l'environnement librement.


Cette image pourra également servir dans un second temps pour réaliser des
tests automatiques à travers le pipeline de Gitlab. Avec cette solution,
il serait possible de vérifier la non-régression après chaque commit.

Cette image permet avant tout une compilation croisée et un lancement
rapide de l'OS sans altérer le système invité.

## Dépendances

* Programme Docker;

## Image RISC-V

Cette image est basée sur Debian et contient :

* riscv-gnu-toolchain
* riscv-spike
* riscv-qemu CEP

## Récupérer l'image du projet

```shell
$ make pull
```

## Makefile

* dl ou pull : télécharge l'image depuis le site de Docker hub (<2Go)
* bash : lance un bash dans un container Docker;
* clean : supprime l'image RISC-V dans Docker.
* build : construit l'image Docker depuis le fichier Dockerfile. Va
télécharger Debian puis clonner et compiler les sources.
* CMD=ma_commande exec : Exécute la commande CMD dans un conteneur Docker

## Compilation du projet

Dans le dossier src du projet, le Makefile contient également des
targets particulières dans le but de compiler, télécharger et lancer la
compilation du projet.


```shell
$ cd src/
$ make dk-all
$ make dk-go
```
