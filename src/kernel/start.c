/*
 * Projet PCSEA RISC-V
 *
 * Benoît Wallon <benoit.wallon@grenoble-inp.org> - 2019
 * Mathieu Barbe <mathieu@kolabnow.com> - 2019
 *
 * See license for license details.
 */

#include "stdio.h"
#include "assert.h"
#include "stddef.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

#include "drivers/splash.h"
#include "tests/tests.h"

int kernel_start()
{
	splash_screen();
	splash_vga_screen();

	puts("\n## Hello world! ##\n");

exit(kernel_tests(NULL));
}
