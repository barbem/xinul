/* VGA fonts */

#include "stdint.h"
#include "stdlib.h"
#include "assert.h"

#include "drivers/auxval.h"

#include "font.h"
#include "vga.h"

static uint32_t *vga_base_addr = NULL;

static void render(char *bitmap, uint32_t x, uint32_t y, uint32_t color)
{
	uint32_t dx, dy;
	for (dy = 0; dy < 8; dy++)
	{
		for (dx = 0; dx < 8; dx++)
		{
			if(bitmap[dy] & 1 << dx)
				*(volatile uint32_t *) (vga_base_addr + (y + dy) * 1280 + x + dx) = color;
		}
	}
}

static void uni_color(uint32_t color) {
	for (int i = 0; i < 1280 * 768; i++)
		*(volatile uint32_t *) (vga_base_addr + i) = color;
}

static void vga_init() {
	vga_base_addr = (uint32_t *) getauxval(VGA_CTRL_ADDR);
	assert(vga_base_addr != NULL);
}

vga_device_t vga_none = {0};
vga_device_t vga_standard = {
	.init = vga_init,
	.render = render,
	.uni_color = uni_color,
};

vga_device_t *vga_dev = &vga_none;

void register_vga(vga_device_t *dev)
{
	vga_dev = dev;
	if(dev->init)
		dev->init();
}
