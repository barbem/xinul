#pragma once

typedef struct vga_device {
	void (*init)();
	void (*render)(char *, uint32_t, uint32_t, uint32_t);
	void (*uni_color)(uint32_t);
} vga_device_t;

/*
 * VGA global variable
 * This variable is useful to call VGA functions.
 */
extern vga_device_t *vga_dev;

static inline void vga_render(char *bitmap, uint32_t x, uint32_t y, uint32_t color) {
	if (vga_dev->render != NULL)
		return vga_dev->render(bitmap, x, y, color);
}

static inline void vga_uni_color(uint32_t color) {
	if (vga_dev->uni_color != NULL)
		return vga_dev->uni_color(color);
}

/*
 * VGA drivers
 */
extern vga_device_t vga_none;
extern vga_device_t vga_standard;

void register_vga(vga_device_t *dev);
