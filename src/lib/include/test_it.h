/*******************************************************************************
 * test_it() est placée dans les tests pour que le timer / clavier puissent être prise en compte quand cela est nécessaire.
 *
 * Cette fonction  est utile en mode kernel car les interruptions supervisor sont désactivées.
 * Ainsi, test_it() active briévement les interruptions au moment voulu.
 * Cela permêt d'éviter d'être interrompu dans une section critique telle qu'un contexte switch ou la création d'un processus.
 *
 * En mode user, test_it() ne sert à rien car les interruption supervidor sont toujours activées.
 * Il n'y a pas de section critique en mode user car l'ensemble du contexte ssera sauvegardé dans le trap handler.
 ******************************************************************************/

#pragma once

#ifdef USER_SPACE

#define test_it(void)

#else

#include "riscv.h"
#include "encoding.h"
#include "stdint.h"

static inline void test_it()
{
	uint64_t tmp_status;

	tmp_status = csr_read(sstatus);
	csr_set(sstatus, MSTATUS_SIE);
	__asm__ __volatile__("nop");
	csr_write(sstatus, tmp_status);
}

#endif
