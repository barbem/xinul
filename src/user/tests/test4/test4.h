/*******************************************************************************
 * Test 4
 *
 * Boucles d'attente active (partage de temps)
 * chprio()
 * kill() de processus de faible prio
 * kill() de processus deja mort
 ******************************************************************************/
#ifndef _TEST4_H_
#define _TEST4_H_

static const int loop_count0 = 1000;
static const int loop_count1 = 2000;

#endif /* _TEST4_H_ */
