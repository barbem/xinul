#include "stdio.h"
#include "assert.h"

#include "syscall.h"

#include "test1.h"

int main(void *arg) {
        printf("1");
        assert((long int) arg == DUMMY_VAL);
        return 3;
}
