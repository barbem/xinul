#include "stdio.h"
#include "assert.h"

#include "syscall.h"

int main(void *arg)
{
        (void)arg;
        wait_clock(current_clock() + 2);
        printf(" not killed !!!");
        assert(0);
        return 1;
}
